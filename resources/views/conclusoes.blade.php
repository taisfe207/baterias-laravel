   @extends('templates.base')
   @section('conteudo')
    <main>
        <h1>Conclusões:</h1>
        <hr>
        <p>Por fim, concluímos então que esse trabalho foi de suma importância para nosso aprendizado e que, com certeza, estará presente em nossos futuros projetos.
            Através dessa atividade obtemos um maior conhecimento sobre medições de tensão com carga e sem carga, resistência de pilhas e baterias e cargas elétricas. Aprendemos com maiores detalhes sobre como criar site na web, como personalizar o mesmo e a construir tabelas.
        </p>
        <img class="bateria" src="../imgs/fim.webp"alt="conclusao">
    </main>

    @endsection
    @section('rodape')
       <h4>Rodapé da pagina principal </h4>
       @endsection