    @extends('templates.base')
    @section('conteudo')
    <main>
        <h1>Procedimentos:</h1>
        <hr>
        <h2>Materiais:</h2>
        <p> 
            Para efetuarmos as mediçoes, utilizamos:
            <p>*1 Multímetro</p>
            <img class="bateria" src="../imgs/multimetro.webp" alt="multimetro">
            <p>*Pilhas </p>
            <img class="bateria" src="../imgs/pilhas.webp" alt="pilhas">
            <p>*Baterias</p>
            <img class="bateria" src="../imgs/baterias.webp" alt="baterias">
            <p>*Resistor de carga</p>
            <img class="bateria" src="../imgs/resistor.webp" alt="resistor">
            <p>*caderno e caneta para anotaçoes</p>
            <img class="bateria" src="../imgs/caderno.webp" alt="caderno">
        </p>


    
        <h3>Mediçoes:</h3>

        <p>Medimos a pilha usando o multímetro na tensão, medindo inicialmente sem o resistor inserido, e depois com o resistor conectado. Esse procedimento é realizado para determinar a resistência interna da pilha.</p>

        <p>Ao medir a tensão da pilha sem o resistor, obtemos o valor da tensão em circuito aberto, ou seja, quando não há corrente fluindo através dela. Em seguida, adicionamos um resistor conhecido ao circuito e medimos novamente a tensão. Ao utilizar o resistor, criamos uma carga no circuito, o que resulta em uma queda de tensão maior do que a medida em circuito aberto.</p>
        <p>Para medir a bateria, seguimos um procedimento semelhante. Primeiro, medimos a tensão da bateria sem qualquer carga ou resistor conectado. Em seguida, adicionamos um resistor conhecido ao circuito e realizamos uma nova medição da tensão. Novamente, a diferença entre as duas leituras de tensão nos permite calcular a corrente que está sendo fornecida pela bateria.

        <p>Esse processo de medição com e sem um resistor é essencial para entender o comportamento da pilha ou bateria em diferentes situações de carga. Ao calcular a resistência interna e a capacidade de fornecer corrente, podemos avaliar o desempenho da pilha ou bateria em diversas aplicações e garantir o seu uso adequado em circuitos elétricos e eletrônicos.</p>
             
        <img class="bateria" src="../imgs/mediçao.webp" alt="medição">
        
    </main>
    
    @endsection
    @section('rodape')
       <h4>Rodapé da pagina principal </h4>
       @endsection